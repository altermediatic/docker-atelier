#!/bin/bash -eux
# ref. https://github.com/matrix-org/synapse/blob/develop/docs/postgres.md#set-up-database

dropdb -U synapse synapse
createdb -U synapse --encoding=UTF8 --locale=C --template=template0 --owner=synapse synapse
