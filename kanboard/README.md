# Kanboard

https://kanboard.org

## Configure
```
export DB_PASSWORD=$(openssl rand -base64 32|sed 's/[+/=]//g')
echo POSTGRES_PASSWORD=$DB_PASSWORD >> .env
echo DATABASE_URL=postgres://kanboard:$DB_PASSWORD@db/kanboard >> .env
```

## Deploy
```
docker compose up -d
```

## First login

Go to your browser and login with admin/admin
**DO NOT FORGET** to change the user & password after first login

## Migration from Postgres 10 to 14

- First backup DB in version 10
```
docker compose exec -T db sh -c 'PGPASSWORD="${POSTGRES_PASSWORD}" pg_dumpall -c -U ${POSTGRES_USER}' > "./db_v10.sql"
```
- IMPORTANT: Check the content of the SQL file to make sure it is not empty
```
docker compose down
```
- Here you should backup all the directories of your kanboard application
```
rm -rf ${CHATONS_ROOT_DIR}/${CHATONS_SERVICE}/db
docker compose up -d db
```
- Check in the docker logs that the DB is up and running. You should find something like this: database system is ready to accept connections
```
docker compose exec -T db sh -c 'PGPASSWORD="${POSTGRES_PASSWORD}" psql -U ${POSTGRES_USER} template1' < ./db_v10.sql
docker compose rm -s -f db
docker compose up -d
```

- Now you need to connect to the DB
```
docker-compose exec db sh
```
- You are now inside the DB container
```
PGPASSWORD="${POSTGRES_PASSWORD}" psql kanboard kanboard
select * from pg_shadow;
```

- It will show you normally 1 line for the kanboard user (if you have more users, you need to perform the next command for each of them)
```
ALTER ROLE kanboard WITH PASSWORD 'put here the password of the kanboard user'
```

- Check that the change is properly done
```
select * from pg_shadow;
```
- you should now see the passwd field starting with SCRAM-SHA-256$4096:
- you might have to restart the service with
```
docker compose down
docker compose up -d
```

